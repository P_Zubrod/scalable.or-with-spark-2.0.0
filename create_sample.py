# -*- coding: utf-8 -*-
##################
#author: Paul Zubrod
#name: create_sample
##description: Create a data sample for Open Refine
#################################

#####
import csv, os,sys


def sample_data(file_path,output="sample.csv", n=500):
    outfileno = 1
    outfile = None
    filename = os.path.splitext(os.path.basename(file_path))[0]
    outfilename = output
    try:
        outfile = open(outfilename, 'w')
        print "sample data successful created"
        print "Filename:  " + outfilename
    except:
        exit("Error while creating sample file")

    with open(file_path, 'r') as infile:
        writer = csv.writer(outfile)
        for index, row in enumerate(csv.reader(infile)):
            if index == n:
                break
            writer.writerow(row)


if __name__ == "__main__":
    if len(sys.argv) <2:
        exit("too few arguments")
    if len(sys.argv) == 2:
        sample_data(sys.argv[1])
    elif len(sys.argv) == 3:
        sample_data(sys.argv[1],sys.argv[2])

