###################
#author: Paul Zubrod
#description:
#configures your Spark installation for Scalable OR Programm
########################

import os, sys, shutil

def set_environment_variable(spark_home):
    os.environ["SPARK_HOME"] = spark_home
    os.environ["PYTHONPATH"] = spark_home + "/python"
    print "SPARK_HOME = " + os.environ["SPARK_HOME"]
    print "PYTHONPATH = " + os.environ["PYTHONPATH"]
    print "Environmentvariables are set!"


if __name__ == "__main__":
    try:
        print os.environ["SPARK_HOME"]
        spark_home = os.environ["SPARK_HOME"]
    except KeyError:
        if len(sys.argv) < 2:
            exit("1 argument: Path to your Spark folder")
        else:
            spark_home = sys.argv[1]



