# -*- coding: utf-8 -*-
##################
#author: Paul Zubrod
#name: Scalable Openrefine Interpreter
##description: Apply Open Refine Programm to CSV Data
#################################

def usage(msg):
    print """
    ARGS
        [CSV] path to CSV
        [CMD] path to OpenRefine Programm
        [Spark_Home]* optional path to Spark folder


    """
    exit(msg)

from pyspark import SparkContext,SparkConf
from pyspark.sql import *
import sys,os,csv,json
from manager import *
from methods import *

#set environmentvariable if wanted
if len(sys.argv) == 4:
    from config import set_environment_variable
    set_environment_variable(sys.argv[3])



#Change Warehouse dir when using Linux
spark = SparkSession.builder.master("local").appName("Scalable OR").config("spark.sql.warehouse.dir", "file:///c:/tmp/spark-warehouse").getOrCreate()
#Create SparkContext
print spark


#import data/log/sample
def import_data_toDF(f):
    df = spark.read.option("header", "true").format(f.split(".")[-1]).load(f)
    return df


def export_data_to_csv(df, path):
    df.write.csv('mycsv.csv')


def load_OR_programm(json_file_path):
    with open(json_file_path) as json_file:
        json_data = json.load(json_file)
    return json_data



def sort_column(data,col,order="asc"):
    if(order == "asc"):
        return data.sort(data[col].asc())
    elif(order == "desc"):
        return data.sort(data[col].asc())



if __name__ == "__main__":
    if len(sys.argv) < 3:
        usage("Arguments are missing")

    if os.path.isabs(sys.argv[1]):
        dataframe = import_data_toDF(sys.argv[1])
        jfp = sys.argv[2]
    else:
        dataframe = import_data_toDF(os.getcwd() + "/" + sys.argv[1])
        jfp = os.getcwd() + "/" + sys.argv[2]
    size = dataframe.count()
    dataframe.show()
    print dataframe.count()

    #openrefine programm
    commands = load_OR_programm(jfp)
    for cmd in commands:
        dataframe = MethodsManager.call(cmd, dataframe,sql_ctx = spark)

    dataframe.show()
    dataframe.printSchema()
    print dataframe.count()


    #further options
    prompt = ""
    while prompt != "y":
        prompt = raw_input("sort? (y/n) \n")
        if prompt == "n":
            break
        if prompt == "y":
            column = raw_input("Column you want sort by \n")
            try:
                dataframe = sort_column(dataframe,column)
                dataframe.show()
            except:
                print "Column doesn't exist"

    prompt = ""
    while prompt != "y":
        prompt = raw_input("Export? (y/n) \n")
        if prompt == "n":
            break
        if prompt == "y":
            filename = raw_input("filename \n")
            export_data_to_csv(dataframe,filename)

    dataframe.show()
